class UsersController < ApplicationController
  def new

  end

  def create
    @user = User.new(post_params)

    @user.save
    redirect_to @user
  end

  def show
    @user = User.find(params[:id])
  end

  def edit

  end

  def index
    @users = User.all
  end

  private

  def post_params
    params.require(:post).permit(:title, :body)
  end
end
