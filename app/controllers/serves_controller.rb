class ServesController < ApplicationController
  def new
    @serve = Serve.new
  end

  def index
    @serves = Serve.all
  end

  def create
    @serve = Serve.new(serve_params)
    @serve.save
  end




  private

  def serve_params
    params.require(:serve).permit(:player, :game, :level)
  end
end
