class Dig < ApplicationRecord
  has_many :players, foreign_key: "player"
  has_many :games, foreign_key: "game"


  def player_name
    player = Player.find(self.player)
    player.name
  end

  def game_date
    game = Game.find(self.game)
    game.created_at
  end
end
