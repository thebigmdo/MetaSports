let toBeSwapped;

let boxes = Array.from(document.getElementsByClassName("box"));
boxes.forEach(box => {
    box.draggable = true;
    box.ondragstart = drag(event);
});

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev, item) {
    ev.dataTransfer.setData("text", ev.target.id);
    toBeSwapped = item;
}

function drop(ev, item) {
    ev.preventDefault();

    let swapHTML = toBeSwapped.innerHTML;
    let swapClass = toBeSwapped.className;
  //let swapInfo = toBeSwapped.info;

    toBeSwapped.innerHTML = item.innerHTML;
    item.innerHTML = swapHTML;

    toBeSwapped.className = item.className;
    item.className = swapClass;

  //toBeSwapped.info = item.info;
  //item.info = swapInfo;
    // You should also copy any other relavent attributes like "id" or "onclick"
}

function popUp(id)
{
  document.getElementById(id).style.display = "flex";
}

function closePopUp(id)
{
  document.getElementById(id).style.display = "none";
}
