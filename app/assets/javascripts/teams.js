

function displayStat(id, openStat)
{
  if(openStat == null) {
    document.getElementById(id).style.display = "block";
    x = document.getElementsByClassName("stat-button");
    var i
    for(i = 0; i < x.length; i++) {
      console.log(typeof i);
      switch(i) {
        case 0:
            x[i].setAttribute("onClick", `displayStat('dig', '${id}')`);
            break;
        case 1:
            x[i].setAttribute("onClick", `displayStat('pass', '${id}')`);
            break;
        case 2:
            x[i].setAttribute("onClick", `displayStat('serve', '${id}')`);
            break;
        case 3:
            x[i].setAttribute("onClick", `displayStat('hit', '${id}')`);
            break;
        case 4:
            x[i].setAttribute("onClick", `displayStat('block', '${id}')`);
            break;
        case 5:
            x[i].setAttribute("onClick", `displayStat('assist', '${id}')`);
            break;
      }
    }
  }
  else {
    document.getElementById(openStat).style.display = "none";
    document.getElementById(id).style.display = "block";
    x = document.getElementsByClassName("stat-button");
    for(i = 0; i < x.length; i++) {
      switch(i) {
        case 0:
            x[i].setAttribute("onClick", `displayStat('dig', '${id}')`);
            break;
        case 1:
            x[i].setAttribute("onClick", `displayStat('pass', '${id}')`);
            break;
        case 2:
            x[i].setAttribute("onClick", `displayStat('serve', '${id}')`);
            break;
        case 3:
            x[i].setAttribute("onClick", `displayStat('hit', '${id}')`);
            break;
        case 4:
            x[i].setAttribute("onClick", `displayStat('block', '${id}')`);
            break;
        case 5:
            x[i].setAttribute("onClick", `displayStat('assist', '${id}')`);
            break;
      }
    }
  }
};
