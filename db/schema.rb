# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190508062733) do

  create_table "assists", force: :cascade do |t|
    t.integer  "player"
    t.integer  "game"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blocks", force: :cascade do |t|
    t.integer  "player"
    t.integer  "game"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "digs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "player"
    t.integer  "game"
  end

  create_table "games", force: :cascade do |t|
    t.integer  "home_team"
    t.integer  "away_team"
    t.integer  "home_score"
    t.integer  "away_score"
    t.date     "date_of_game"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "hits", force: :cascade do |t|
    t.integer  "player"
    t.integer  "game"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "passes", force: :cascade do |t|
    t.integer  "player"
    t.integer  "game"
    t.integer  "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "players", force: :cascade do |t|
    t.string   "name"
    t.integer  "age"
    t.string   "height"
    t.integer  "team"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "serves", force: :cascade do |t|
    t.integer  "player"
    t.integer  "game"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "level"
  end

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.string   "age"
    t.integer  "team_size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "title"
    t.integer  "team"
    t.integer  "admin"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "f_name"
    t.string   "l_name"
    t.date     "last_login"
    t.date     "date_created"
  end

end
