class CreatePasses < ActiveRecord::Migration[5.0]
  def change
    create_table :passes do |t|
      t.integer :player
      t.integer :game
      t.integer :level

      t.timestamps
    end
  end
end
