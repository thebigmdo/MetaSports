class CreateDigs < ActiveRecord::Migration[5.0]
  def change
    create_table :digs do |t|
      t.references :player, foreign_key: true
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end
