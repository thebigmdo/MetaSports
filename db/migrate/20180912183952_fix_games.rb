class FixGames < ActiveRecord::Migration[5.0]
  def change
    change_table "games" do |t|
      t.change :home_team, :integer
      t.change :away_team, :integer
    end
  end
end
