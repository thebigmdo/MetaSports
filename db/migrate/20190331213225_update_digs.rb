class UpdateDigs < ActiveRecord::Migration[5.0]
  def change
    change_table :digs do |t|
      t.remove :player_id, :game_id, :recorded_at
      t.integer :player
      t.integer :game
    end
  end
end
