class CreateServes < ActiveRecord::Migration[5.0]
  def change
    create_table :serves do |t|
      t.integer :player
      t.integer :game
      t.integer :type

      t.timestamps
    end
  end
end
