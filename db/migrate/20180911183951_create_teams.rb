class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :age
      t.integer :team_size

      t.timestamps
    end
  end
end
